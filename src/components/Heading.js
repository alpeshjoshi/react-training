import "../assets/css/Heading.css";
import Demo from "../assets/css/demo.module.css";
import "../assets/scss/sass-demo.scss";

function Heading(){
    return(
        <div>
            <h1 className="heading">Normal CSS</h1>
            <h2 style={{color:"blue", backgroundColor:"yellow", fontSize:"80px"}}>Inline CSS</h2>
            <h3 className={Demo.heading}>Module CSS</h3>
            <h4 className="heading-sass">SCSS Style</h4>
        </div>
    )
}

export default Heading;