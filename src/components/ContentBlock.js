import Image from "./Image";

function ContentBlock (props){
    return(
        <>
        <h1>This is content block component.</h1>
        <h2>Heading 2 </h2>
        <Image />
        </>
    )
}

export default ContentBlock;