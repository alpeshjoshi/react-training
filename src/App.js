import logo from './logo.svg';
import './App.css';
// import Heading from "./components/Heading";
// import Promo from "./components/Promo";
import Image from "./components/Image";
import ContentBlock from './components/ContentBlock';

function App() {
  return (
    <div>
      <ContentBlock />
    </div>
  );
}

export default App;
